# Build EKS cluster by using a terraform module and deploy it through Gitlab Pipeline. Implement GitOps workflow by installing Gitlab Agent inside a cluster, and automating deploying of Kubernetes manifest files (Deployments, Services,and Ingress Controller)

# ------------------------------------------------------------
## What is a Gitlab Agent and why do we use it?



### The GitLab Agent for Kubernetes ( agentk ) is an active in-cluster component for solving GitLab and Kubernetes integration tasks in a secure and cloud-native way. The agentk communicates to the GitLab Agent Server (KAS) to perform GitOps operations. This means, if you change K8s objects, the agent will detect that changes, and automatically redeploy applications. Here is the official [documentation](https://docs.gitlab.com/ee/user/clusters/agent/install/) what is an agent and how to install it

# ------------------------------------------------------------
## I've automated everything, there's no manual job. In order to run this pipeline, you have to configure the following things:

### 1) Open the main.tf file under the terraform directory and configure terraform backend (line 13-19). We'll save our terraform state file in S3 bucket. For this, we have to create S3 bucket or use an existing one and paste the name of S3 bucket on line 15. In terraform directory, there's also readmy.md file where you can understand about the module itself and how to apply it locally 

### 2) Go to the Gitlab UI. On the left side, click **Infrastructure/Kubernetes cluster** click the blue button **Connect a cluster(agent)** and give the appropriate name and **Register**. 
![](pictures/agent.png) 
### After this, there will be **agent token** and helm command to install the agent. Copy **agent token** and go to the file **.gitlab/pipeline/gitlab-agent.yml** and paste copied token in the variable **AGENT_TOKEN**
![](pictures/token.png)

### 3) On the left side, click the button **Settings/Genreal** and copy **project ID** 
![](pictures/project_id.png) 
### And paste it **.gitlab/agents/eks-agent/config.yaml**
![](pictures/id.png)

### 4) On the left side, click **Settings/CICD**. Click on the **Variables** and add your AWS cred(access and secret key)
![](pictures/cred.png)

## Project is almost ready. You can trigger the pipeline 

## Additional information:
- All Kubernetes objects located under **K8s** directory
- If you'll change, for example, replica of the deployment, the Gitlab agent will detect the changes and update it within the 5-10 seconds
- Configuration of the Gitlab Pipeline is **.gitlab-ci.yml** file. There you can orchestrate the pipeline and add some variables. I've optimized the code and added **include** section in pipeline in order to make it structured. 

![](pictures/include.png)
- I've added hidden stage(line 17-21) to optimize the code and stop to run repeatable things. It will be inherited in other stages. 
![](pictures/hidden.png)
- As you can see, on the line 19-21 I've added **only/changes** feature. This means, The pipeline will be triggered in cases if in **terraform** directory will be some changes.

## In order to check all stuff, you have to log in to EKS. Steps to log in:
- Spin up a new EC2 Instance (with the default configurations)
- Connect to the Instance 
- Install **Kubectl** utility
- Install **AWS CLI**
- Export AWS Cred (access and secret key) then:

```bash
EKS_CLUSTER_NAME=$(aws eks list-clusters --region us-east-1 --query clusters[0] --output text)

echo $EKS_CLUSTER_NAME

aws eks update-kubeconfig --name $EKS_CLUSTER_NAME --region us-east-1
```
- after this you can test by running any kubectl command : **kubectl get svc**

## To check that all containers and ingress are running, run the command:
```
kubectl get all --all-namespaces
```
### It will show all resources on each namespace

## To check the Gitlab agent, you can run following command:
```
kubectl logs -f -l=app=gitlab-agent -n gitlab-agent
```
### After this you can change for exampel the replicas of the one deployment into 2 and commit the changes. Agent should immediately detect and update the replicas into 2 

## In order to test the **Kubernetes Ingress Controller** , first get the DNS of the Load Balancer (you can get by running **kubectl get svc**). And go the Browser and paste it . It should show the NGINX container content. If you will change to **http://dns_name/whoami** it should forward to **whoami** container
