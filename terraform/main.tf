
data "aws_vpc" "default" {
   default = true
} 

data "aws_subnets" "example" {
  filter {
    name = "tag:Name"
    values = [ "test" ]
  }
}

terraform {
  backend "s3" {
    bucket = "efuse-project"
    key    = "state"
    region = "us-east-1"
  }
}



# terraform {
#   required_version = "0.13.7"
# }






module "eks" {
  source = "./modules"

  vpc_id          = data.aws_vpc.default.id
  cluster_name    = var.eks_name
  cluster_version = var.eks_version
  env             = var.env
  subnet_ids      = data.aws_subnets.example.ids

  node_groups = {
    first = {
      node_group_name = "test"
      desired_size    = 6
      max_size        = 6
      min_size        = 4

      ami_type       = "AL2_x86_64"
      instance_types = ["t2.micro"]
    },
  }
}

#===================================================================================================
#  IAM user

resource "aws_iam_access_key" "test_user" {
  user    = aws_iam_user.test_user.name
#   pgp_key = "keybase:some_person_that_exists"
}

resource "aws_iam_user" "test_user" {
  name = "loadbalancer"
  path = "/system/"
}



resource "aws_iam_user_policy" "s3_write_policy" {
  name = "test"
  user = aws_iam_user.test_user.name
  policy = data.aws_iam_policy_document.policy.json
}

data "aws_iam_policy_document" "policy" {
  statement {
    actions = [
      "s3:GetObject",
        "s3:ListBucket",
        "s3:PutObject",
        "s3:PutObjectAcl"
    ]

    resources = [aws_s3_bucket.test_bucket.arn,"${aws_s3_bucket.test_bucket.arn}/*"]
  }

}

output "secret" {
  value = aws_iam_access_key.test_user.encrypted_secret
}
#======================================================
#         S3 bucket

resource "aws_s3_bucket" "test_bucket" {
  bucket = "my-test-bucket-nur12"

  tags = {
    Name        = "My bucket"
    Environment = "Test"
  }
}

resource "aws_s3_bucket_public_access_block" "test" {
  bucket = aws_s3_bucket.test_bucket.id

  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}
